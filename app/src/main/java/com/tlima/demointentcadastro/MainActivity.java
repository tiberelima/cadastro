package com.tlima.demointentcadastro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void abrirPF(View view) {
        startActivity(new Intent(getApplicationContext(), PessoaFisicaActivity.class));
    }

    public void abrirPJ(View view) {
        startActivity(new Intent(getApplicationContext(), PessoaJuridicaActivity.class));
    }
}
